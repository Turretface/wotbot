#!/bin/bash
source data/crowdin.config
pybabel extract -o messages.pot .
curl -F "files[/WotBot/messages.pot]=@messages.pot" https://api.crowdin.com/api/project/WotBot/update-file?key=$key
