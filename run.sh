#!/bin/bash
wait_time=1
while true;
do
        SECONDS=0
	    python wotbot.py;
        if (( SECONDS > 60 )); then
            wait_time=1
        fi
        echo "sleep " $wait_time
        sleep $wait_time
        wait_time=$((wait_time+1))
done
