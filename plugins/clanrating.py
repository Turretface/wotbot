import discord
from discord.ext import commands
import asyncio
from operator import itemgetter
import datetime
import math
import humanize
import time
from tabulate import tabulate


class ClanStats(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True)
    # @asyncio.coroutine
    async def clanrating(self, ctx, *, clan_name: str = None):
        """Clan rating. Clan name or empty for current user's clan"""
        self.bot.logger.info(("clan rating:", clan_name))
        color_list = "javascript"
        tstart = time.time()
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        await self.bot.dc.typing(ctx)

        clan_id = None
        if clan_name is None:
            player, region, token, ownuser = await self.bot.wg.get_local_player(
                clan_name, ctx
            )

            if not player:
                return
            self.bot.logger.info(("clan activity:", clan_name))
            if player:
                player_id = str(player[0]["account_id"])
                player_nick = player[0]["nickname"]
                # clan=self.bot.wg.wotb_servers[region].clans.accountinfo(account_id=player_id)
                clan = await self.bot.wg.get_wg(
                    region=region,
                    cmd_group="clans",
                    cmd_path="accountinfo",
                    parameter="account_id={}".format(player_id),
                )
                if clan[player_id] is None:
                    await ctx.send(
                        _("Player `{player}@{region}` has no active clan.").format(
                            player=player_nick, region=region
                        )
                    )
                    return
                if clan[player_id]["clan_id"] is None:
                    await ctx.send(
                        _("Player `{player}@{region}` has no active clan.").format(
                            player=player_nick, region=region
                        )
                    )
                    return
                # clan=wotb.clans.accountinfo(account_id=player_id)
                clan_id = str(clan[player_id]["clan_id"])
                # process_clan(clan_id)
            else:
                out = await self.bot.wg.search_player_a(ctx, clan_name)
                await self.bot.dc.not_found_msg(ctx, out)
        else:
            self.bot.logger.debug("got clan name {}".format(clan_name))
            # clan_list=wotb.clans.list(fields="clan_id,tag,name",search=clan_name)
            clan_list, region = await self.bot.wg.get_clan_a(clan_name)
            self.bot.logger.debug("got this from search: {}".format(clan_list))
            if clan_list:
                # clan_id=str(clan[player_id]['clan_id'])
                clan_id = str(clan_list[0]["clan_id"])
                # process_clan(clan_id)
            else:
                await ctx.send(_("Clan not found"))
        if clan_id is not None:
            # clan_info=self.bot.wg.wotb_servers[region].clans.info(clan_id=clan_id, extra="members", fields="tag, name, emblem_set_id, motto,members.account_name")
            clan_info = await self.bot.wg.get_wg(
                region=region,
                cmd_group="clans",
                cmd_path="info",
                parameter="clan_id={}&extra=members&fields=tag, name, emblem_set_id, motto,members.account_name".format(
                    clan_id
                ),
            )

            clan_tag = clan_info[clan_id]["tag"]
            clan_name = clan_info[clan_id]["name"]
            clan_emblem = clan_info[clan_id]["emblem_set_id"]
            clan_motto = clan_info[clan_id]["motto"]
            # clan_members_count=clan_info[clan_id]['members_count']
            # clan_members_list_helper=clan_info[clan_id]['members_ids']
            clan_members_list = []
            clan_members_list_error = []
            clan_members_list_uncalibrated = []
            clan_members = clan_info[clan_id]["members"]

            embed = discord.Embed(
                title=clan_tag, description=clan_name, colour=234, type="rich"
            )
            # embed.add_field(name='\uFEFF', value="```{}```".format(o))
            embed.set_thumbnail(
                url="http://dl-wotblitz-gc.wargaming.net/icons/clanIcons1x/clan-icon-v2-{}.png".format(
                    clan_emblem
                )
            )
            # embed.set_thumbnail(url="http://glossary-eu-static.gcdn.co/icons/wotb/current/achievements/markOfMastery{}.png".format(mark))
            embed.set_footer(
                text="{}".format(clan_motto),
                icon_url="http://dl-wotblitz-gc.wargaming.net/icons/clanIcons1x/clan-icon-v2-{}.png".format(
                    clan_emblem
                ),
            )
            try:
                await ctx.send(content=None, embed=embed)
            except discord.Forbidden:
                self.bot.logger.warning(
                    "Please enable Embed links permission for wotbot."
                )
                await ctx.send(
                    content=_("Please enable Embed links permission for wotbot.")
                )

            # msg= await ctx.send(out)
            # await ctx.trigger_typing()
            for clan_member_id, clan_member_name in clan_members.items():
                # return(player_rating,player_rating_place,player_rating_league)
                player_rating, player_rating_place, player_rating_league = await self.bot.wg.get_rating(
                    clan_member_id, region, ctx, score=2
                )

                if isinstance(player_rating, int):
                    clan_members_list.append(
                        [
                            clan_member_name["account_name"],
                            player_rating,
                            player_rating_place,
                            player_rating_league,
                        ]
                    )
                elif "Calibration Battles left" in player_rating:
                    clan_members_list_uncalibrated.append(
                        clan_member_name["account_name"]
                    )
                else:
                    clan_members_list_error.append(clan_member_name["account_name"])

            await ctx.send(
                "```{}\n{} {}@{}\n{: <16} {: >8} {: >8} {: <8}```".format(
                    color_list,
                    _("Clan rating for"),
                    clan_name,
                    region,
                    _("playername"),
                    _("Points"),
                    _("Place"),
                    _("League"),
                )
            )
            # clan_members_list=sorted(clan_members_list, key=lambda x: (x[3],-x[5]))
            clan_members_list = sorted(clan_members_list, key=lambda x: (x[2]))
            # print(clan_members_list)
            out_list = "```{color}\n".format(color=color_list)
            for i in clan_members_list:
                out_list += "{: <16} {: >8} {: >8} {: <8}\n".format(
                    i[0][:16], i[1], i[2], i[3]
                )
                # out_list+="{}".format(tabulate(clan_members_list,headers=["Name", "Points","Place","League"],tablefmt="grid",stralign="right",numalign="right"),join)
                # out_list+="{: <16} {} {} {}\n".format(i[0][:16],i[1],i[2],i[3])
            out_list += "```"
            out_total = out_list
            self.bot.logger.debug(len(out_total))
            if len(out_total) > 2000:
                # just no time and energy to do better now
                out_list = "```{color}\n".format(color=color_list)
                for i in clan_members_list[: math.ceil(len(clan_members_list) / 2.0)]:
                    # out_list+="{: <16} {: >8} {: >8} {: >8}\n".format(i[0][:16],i[1],i[2],i[3])
                    out_list += "{: <16} {} {} {}\n".format(i[0][:16], i[1], i[2], i[3])
                out_list += "```"
                await ctx.send(out_list)
                out_list = "```{color}\n".format(color=color_list)
                for i in clan_members_list[
                    len(clan_members_list) - math.floor(len(clan_members_list) / 2.0) :
                ]:
                    out_list += "{: <16} {} {} {}\n".format(i[0][:16], i[1], i[2], i[3])
                out_list += "```"
                await ctx.send(out_list)
            else:
                self.bot.logger.debug(len(out_total))
                await ctx.send(out_total)
            # await ctx.send(out_total)

            # await ctx.send(", ".join(clan_members_list_uncalibrated))
            # await ctx.send(", ".join(clan_members_list_error))

            # self.bot.logger.debug(clan_members_list)
            print(humanize.naturaldelta(tstart - time.time()))
            self.bot.logger.info("end of clan stats task")


def setup(bot):
    bot.add_cog(ClanStats(bot))
