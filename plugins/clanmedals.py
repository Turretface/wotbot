import discord
from discord.ext import commands
import asyncio
from operator import itemgetter
import humanize
import time


class ClanStats(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True, aliases=["calnmedals"])
    # @asyncio.coroutine
    async def clanmedals(self, ctx, *, clan_name: str = None):
        """Medal counts for clan members. Clan name or empty for current user's clan"""
        tstart = time.time()
        self.bot.logger.info(("clanmedals:", clan_name))
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        await self.bot.dc.typing(ctx)

        meds = [
            "heroesOfRassenay",
            "medalLafayettePool",
            "medalKolobanov",
            "medalRadleyWalters",
            "mainGun",
        ]
        # achievements_wg=self.bot.wg.wotb_servers["eu"].encyclopedia.achievements()
        achievements_wg = await self.bot.wg.get_all_achievements()

        clan_id = None
        if clan_name is None:
            player, region, token, ownuser = await self.bot.wg.get_local_player(
                clan_name, ctx
            )

            if not player:
                return

            self.bot.logger.info(("clanmedals:", clan_name))
            if player:
                player_id = str(player[0]["account_id"])
                player_nick = player[0]["nickname"]
                # clan= self.bot.wg.wotb_servers[region].clans.accountinfo(account_id=player_id)
                clan = await self.bot.wg.get_wg(
                    region=region,
                    cmd_group="clans",
                    cmd_path="accountinfo",
                    parameter="account_id={}".format(player_id),
                )
                if clan.get(player_id, None) is None:
                    await ctx.send(
                        "Player `{player}@{region}` has no active clan.".format(
                            player=player_nick, region=region
                        )
                    )
                    return
                if clan[player_id].get("clan_id", None) is None:
                    await ctx.send(
                        "Player `{player}@{region}` has no active clan.".format(
                            player=player_nick, region=region
                        )
                    )
                    return
                # clan=wotb.clans.accountinfo(account_id=player_id)
                clan_id = str(clan[player_id]["clan_id"])
                # process_clan(clan_id)
            else:
                out = await self.bot.wg.search_player_a(ctx, clan_name)
                await self.bot.dc.not_found_msg(ctx, out)
        else:
            self.bot.logger.debug("got clan name {}".format(clan_name))
            # clan_list=wotb.clans.list(fields="clan_id,tag,name",search=clan_name)
            clan_list, region = await self.bot.wg.get_clan_a(clan_name)
            # self.bot.logger.debug("got this from search: {}".format(clan_list))
            if clan_list:
                # clan_id=str(clan[player_id]['clan_id'])
                clan_id = str(clan_list[0]["clan_id"])
                # process_clan(clan_id)
            else:
                await ctx.send("clan not found")

        players_names_dict = {}
        if clan_id is not None:
            # clan_info=self.bot.wg.wotb_servers[region].clans.info(clan_id=clan_id)
            clan_info = await self.bot.wg.get_wg(
                region=region,
                cmd_group="clans",
                cmd_path="info",
                parameter="clan_id={}".format(clan_id),
            )
            clan_tag = clan_info[clan_id]["tag"]
            clan_name = clan_info[clan_id]["name"]
            clan_emblem = clan_info[clan_id]["emblem_set_id"]
            clan_motto = clan_info[clan_id]["motto"]
            clan_members_count = clan_info[clan_id]["members_count"]
            clan_members_list_helper = clan_info[clan_id]["members_ids"]

            embed = discord.Embed(
                title=clan_tag,
                description="{name}\n{members}: {cnt}".format(
                    name=clan_name, members=_("members"), cnt=clan_members_count
                ),
                colour=234,
                type="rich",
            )
            # embed.add_field(name='\uFEFF', value="```{}```".format(o))
            embed.set_thumbnail(
                url="http://dl-wotblitz-gc.wargaming.net/icons/clanIcons1x/clan-icon-v2-{}.png".format(
                    clan_emblem
                )
            )
            # embed.set_thumbnail(url="http://glossary-eu-static.gcdn.co/icons/wotb/current/achievements/markOfMastery{}.png".format(mark))
            embed.set_footer(
                text="{}".format(clan_motto),
                icon_url="http://dl-wotblitz-gc.wargaming.net/icons/clanIcons1x/clan-icon-v2-{}.png".format(
                    clan_emblem
                ),
            )
            try:
                await ctx.send(content=None, embed=embed)
            except discord.Forbidden:
                self.bot.logger.warning(
                    "Please enable Embed links permission for wotbot."
                )
                await ctx.send(
                    content=_("Please enable Embed links permission for wotbot.")
                )

            await self.bot.dc.typing(ctx)

            clan_players = ",".join(str(x) for x in clan_members_list_helper)
            # return self.bot.wg.get_players_by_ids(clan_players,region)
            clan_players_returned, region = await self.bot.wg.get_players_by_ids_a(
                clan_players, region
            )
            players_achiev = await self.bot.wg.get_wg(
                region=region,
                cmd_group="account",
                cmd_path="achievements",
                parameter="account_id={}".format(clan_players),
            )
            # print(players_achiev)
            # future=self.bot.loop.run_in_executor(None,a)
            # clan_players_returned, region =await future
            for clan_member, player in clan_players_returned.items():
                if player is not None:
                    if player["nickname"] not in players_names_dict:
                        players_names_dict[player["nickname"]] = []
                        player_id = str(player["account_id"])
                        player_nick = player["nickname"]
                        # player_achiev=self.bot.wg.wotb_servers[region].account.achievements(account_id=player_id)
                        # player_achiev = await self.bot.wg.get_wg(region=region,cmd_group="account",cmd_path="achievements",parameter="account_id={}".format(player_id))
                        player_achiev = {player_id: players_achiev[player_id]}
                        # print(player_id, player_achiev)
                        # self.bot.logger.debug(player_achiev)
                        players_names_dict[player_nick] = [player_id, player_achiev]
            self.bot.logger.debug(players_names_dict)
            print(humanize.naturaldelta(tstart - time.time()))
            for i in meds:
                lp = []
                for p in players_names_dict:
                    res = players_names_dict[p][1][players_names_dict[p][0]][
                        "achievements"
                    ].get(i, 0)
                    if res:
                        lp.append(
                            [
                                p,
                                players_names_dict[p][1][players_names_dict[p][0]][
                                    "achievements"
                                ].get(i, 0),
                            ]
                        )
                lpsorted = sorted(lp, key=itemgetter(1), reverse=True)
                o = ""
                for p in lpsorted:
                    o += "{: <20} {: >3}\n".format(p[0], p[1])
                    self.bot.logger.debug(o)
                    # self.bot.logger.debug((p, players_names_dict[p][1][players_names_dict[p][0]]['achievements'].get(i,0)))
                embed = discord.Embed(
                    title="{}".format(achievements_wg[i]["name"]),
                    description="```{}```\n                                                                                                  \uFEFF".format(
                        o
                    ),
                    colour=234,
                    type="rich",
                )
                # embed.add_field(name='\uFEFF', value="```{}```".format(o))
                embed.set_thumbnail(url=achievements_wg[i]["image"])
                embed.set_footer(text=achievements_wg[i]["description"])
                try:
                    await ctx.send(content=None, embed=embed)
                except discord.Forbidden:
                    self.bot.logger.warning(
                        "Please enable Embed links permission for wotbot."
                    )
                    await ctx.send(
                        content=_("Please enable Embed links permission for wotbot.")
                    )
                    break
        print(humanize.naturaldelta(tstart - time.time()))

        # await bot.add_reaction(msg,'\U0001F44D') #test of votable emoji → "reaction"


def setup(bot):
    bot.add_cog(ClanStats(bot))
