import discord
from discord.ext import commands
import asyncio
from operator import itemgetter
import random

# import requests
import json
from tabulate import tabulate
from collections import Counter
import pickle
import datetime
from pathlib import Path
import humanize
import aiohttp
import re

rating_region = {
    "eu": ["eu", "en"],
    "na": ["com", "en"],
    "ru": ["ru", "ru"],
    "asia": ["asia", "en"],
}


class UserStats(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True, hidden=False, aliases=["listsaved"])
    # @asyncio.coroutine
    async def liststats(self, ctx):
        """List names of players you keep track of"""
        self.bot.logger.info("liststats")
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        await self.bot.dc.typing(ctx)

        DATA_DIR = Path("./stats/")
        players = []
        for fpath in DATA_DIR.glob("{}-*.pickle".format(ctx.message.author.id)):
            # print(fpath, fpath.read_text())
            # print(fpath)
            pattern = re.compile(".*-([0-9]{8,})(\.pickle)")
            if pattern.match(str(fpath)):
                players.append(pattern.match(str(fpath)).groups()[0])
        # print(len(players))
        if len(players):
            await ctx.send(
                _("Processing {} names, this will take a moment.").format(len(players))
            )
            await self.bot.dc.typing(ctx)
        else:
            await ctx.send(_("Seems like no saved stats found."))
            return
        names = []
        names_str = []
        if players:
            for player_id in players:
                player, region = await self.bot.wg.get_player_by_id_a(player_id)
                # def a():
                #    self.bot.wg.get_player_by_id(player_id)
                # future = self.bot.loop.run_in_executor(None, a)
                # d= await future
                # print(d)

                if player:
                    player = player[player_id]
                    if player:
                        player_name = player.get("nickname", "no name")
                        names.append(["`{}@{}`".format(player_name, region)])
                        names_str.append("`{}@{}`".format(player_name, region))
                        if len(str(names)) > 500:
                            output = tabulate(names, tablefmt="grid")
                            # print(output)
                            embed = discord.Embed(
                                title="Stats saved for:",
                                description="```{}```".format(output),
                                colour=234,
                                type="rich",
                            )
                            embed.set_footer(text="{}".format(" ".join(names_str)))
                            try:
                                msg = await ctx.send(content=None, embed=embed)
                            except discord.Forbidden:
                                self.bot.logger.warning(
                                    "Please enable Embed links permission for wotbot."
                                )
                                await ctx.send(
                                    content=_(
                                        "Please enable Embed links permission for wotbot."
                                    )
                                )

                            names = []
                            names_str = []
        # print((names_str))
        output = tabulate(names, tablefmt="grid")
        # print(len(output))
        embed = discord.Embed(
            title=_("Stats saved for:"),
            description="```{}```".format(output),
            colour=234,
            type="rich",
        )
        embed.set_footer(text="{}".format(" ".join(names_str)))
        try:
            msg = await ctx.send(content=None, embed=embed)
        except discord.Forbidden:
            self.bot.logger.warning("Please enable Embed links permission for wotbot.")
            await ctx.send(
                content=_("Please enable Embed links permission for wotbot.")
            )


def setup(bot):
    bot.add_cog(UserStats(bot))
