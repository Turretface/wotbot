import discord
from discord.ext import commands
import asyncio
from operator import itemgetter
import datetime
from collections import Counter
import math
import humanize
import time

mastery_marks = {
    "4": "Ace Tanker",
    "3": "1st Class",
    "2": "2nd Class",
    "1": "3rd Class",
    "0": "No mastery",
}

ppl_to_wg = {"m": "4", "a": "4", "ace": "4", "1": "3", "2": "2", "3": "1", "0": "0"}


class ClanStats(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(
        pass_context=True,
        aliases=[
            "clanplayerscharts",
            "clanplayers",
            "calntopplayers",
            "calnplayerscharts",
            "calnplayers",
            "clantoplayers",
        ],
    )
    # @asyncio.coroutine
    async def clantopplayers(self, ctx, *, clan_name: str = None):
        """Top clan players per tier: mastery/dmg/wr"""
        tstart = time.time()
        self.bot.logger.info(("clan activity:", clan_name))

        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        await self.bot.dc.typing(ctx)

        color_list = "javascript"
        # all_vehicles=self.bot.wg.wotb_servers["eu"].encyclopedia.vehicles(fields="tier,name")
        all_vehicles = await self.bot.wg.get_all_vehicles()
        clan_id = None
        if clan_name is None:
            player, region, token, ownuser = await self.bot.wg.get_local_player(
                clan_name, ctx
            )

            if not player:
                return
            self.bot.logger.info(("clan players charts:", clan_name))
            if player:
                player_id = str(player[0]["account_id"])
                player_nick = player[0]["nickname"]
                # clan=self.bot.wg.wotb_servers[region].clans.accountinfo(account_id=player_id)
                clan = await self.bot.wg.get_wg(
                    region=region,
                    cmd_group="clans",
                    cmd_path="accountinfo",
                    parameter="account_id={}".format(player_id),
                )
                if clan[player_id] is None:
                    await ctx.send(
                        _("Player `{player}@{region}` has no active clan.").format(
                            player=player_nick, region=region
                        )
                    )
                    return
                if clan[player_id]["clan_id"] is None:
                    await ctx.send(
                        _("Player `{player}@{region}` has no active clan.").format(
                            player=player_nick, region=region
                        )
                    )
                    return
                # clan=wotb.clans.accountinfo(account_id=player_id)
                clan_id = str(clan[player_id]["clan_id"])
                # process_clan(clan_id)
            else:
                out = await self.bot.wg.search_player_a(ctx, clan_name)
                await self.bot.dc.not_found_msg(ctx, out)
        else:
            self.bot.logger.debug("got clan name {}".format(clan_name))
            # clan_list=wotb.clans.list(fields="clan_id,tag,name",search=clan_name)
            clan_list, region = await self.bot.wg.get_clan_a(clan_name)
            self.bot.logger.debug("got this from search: {}".format(clan_list))
            if clan_list:
                # clan_id=str(clan[player_id]['clan_id'])
                clan_id = str(clan_list[0]["clan_id"])
                # process_clan(clan_id)
            else:
                await ctx.send(_("Clan not found"))

        if clan_id is not None:
            await ctx.send(_("...generating stats, this will take some time..."))
            # clan_info=self.bot.wg.wotb_servers[region].clans.info(clan_id=clan_id)
            clan_info = await self.bot.wg.get_wg(
                region=region,
                cmd_group="clans",
                cmd_path="info",
                parameter="clan_id={}".format(clan_id),
            )
            clan_tag = clan_info[clan_id]["tag"]
            clan_name = clan_info[clan_id]["name"]
            clan_emblem = clan_info[clan_id]["emblem_set_id"]
            clan_motto = clan_info[clan_id]["motto"]
            clan_members_count = clan_info[clan_id]["members_count"]
            clan_members_list_helper = clan_info[clan_id]["members_ids"]
            clan_members_list = {}

            embed = discord.Embed(
                title=clan_tag, description=clan_name, colour=234, type="rich"
            )
            # embed.add_field(name='\uFEFF', value="```{}```".format(o))
            embed.set_thumbnail(
                url="http://dl-wotblitz-gc.wargaming.net/icons/clanIcons1x/clan-icon-v2-{}.png".format(
                    clan_emblem
                )
            )
            # embed.set_thumbnail(url="http://glossary-eu-static.gcdn.co/icons/wotb/current/achievements/markOfMastery{}.png".format(mark))
            embed.set_footer(
                text="{}".format(clan_motto),
                icon_url="http://dl-wotblitz-gc.wargaming.net/icons/clanIcons1x/clan-icon-v2-{}.png".format(
                    clan_emblem
                ),
            )
            try:
                await ctx.send(content=None, embed=embed)
            except discord.Forbidden:
                self.bot.logger.warning(
                    "Please enable Embed links permission for wotbot."
                )
                await ctx.send(
                    content=_("Please enable Embed links permission for wotbot.")
                )

            await self.bot.dc.typing(ctx)

            clan_players = ",".join(str(x) for x in clan_info[clan_id]["members_ids"])
            clan_players_returned, region = await self.bot.wg.get_players_by_ids_a(
                clan_players, region
            )
            for clan_member, member_info in clan_players_returned.items():
                # for clan_member in clan_info[clan_id]['members_ids']:
                # member_info=self.bot.wg.wotb_servers[region].account.info(account_id=clan_member)
                # def a():
                #    return self.bot.wg.wotb_servers[region].account.info(account_id=clan_member)

                # future2 = self.bot.loop.run_in_executor(None,a)
                # member_info=await future2
                if member_info is not None:
                    all_tiers = {}
                    # player_tanks=self.bot.wg.wotb_servers[region].tanks.stats(account_id=clan_member, fields="tank_id, all.wins, all.battles, all.damage_dealt")[clan_member]
                    # def b():
                    # return self.bot.wg.wotb_servers[region].tanks.stats(account_id=clan_member, fields="tank_id, all.wins, all.battles, all.damage_dealt") [clan_member]
                    player_tanks = await self.bot.wg.get_wg(
                        region=region,
                        cmd_group="tanks",
                        cmd_path="stats",
                        parameter="account_id={}&fields=tank_id, all.wins, all.battles, all.damage_dealt".format(
                            clan_member
                        ),
                    )
                    player_tanks = player_tanks[clan_member]
                    # future1=self.bot.loop.run_in_executor(None, b)
                    # player_tanks= await future1
                    if player_tanks is not None:
                        clan_members_list[clan_member] = {}
                        clan_members_list[clan_member]["name"] = member_info["nickname"]
                        for tank in player_tanks:
                            if str(tank["tank_id"]) in all_vehicles:
                                tier = all_vehicles[str(tank["tank_id"])]["tier"]
                            else:
                                tier = 0
                                print("not in tanko", tank)
                            if tier not in all_tiers:
                                all_tiers[tier] = tank["all"]
                            else:
                                all_tiers[tier] = dict(
                                    Counter(all_tiers[tier]) + Counter(tank["all"])
                                )
                        clan_members_list[clan_member]["tiers"] = all_tiers

                    all_aces = {
                        0: 0,
                        1: 0,
                        2: 0,
                        3: 0,
                        4: 0,
                        5: 0,
                        6: 0,
                        7: 0,
                        8: 0,
                        9: 0,
                        10: 0,
                    }
                    # player_achievs=self.bot.wg.wotb_servers[region].tanks.achievements(account_id=clan_member, fields="tank_id,achievements")[clan_member]
                    # def c():
                    # return self.bot.wg.wotb_servers[region].tanks.achievements( account_id=clan_member, fields="tank_id,achievements")[clan_member]
                    player_achievs = await self.bot.wg.get_wg(
                        region=region,
                        cmd_group="tanks",
                        cmd_path="achievements",
                        parameter="account_id={}&fields=tank_id,achievements".format(
                            clan_member
                        ),
                    )
                    player_achievs = player_achievs[clan_member]
                    # future3=self.bot.loop.run_in_executor(None, c)
                    # player_achievs= await future3
                    if player_achievs:
                        for tank in player_achievs:
                            if str(tank["tank_id"]) in all_vehicles:
                                tier = all_vehicles[str(tank["tank_id"])]["tier"]
                            else:
                                tier = 0
                            markOfMastery = str(
                                tank["achievements"].get("markOfMastery", "0")
                            )
                            # print("tier",tier)
                            if markOfMastery == "4":  # ace
                                all_aces[tier] = all_aces[tier] + 1
                        clan_members_list[clan_member]["aces"] = all_aces
                        # print(all_aces)

            # print(clan_members_list)

            # start generating max() tables
            max_ace = {}
            max_dmg = {}
            max_wr = {}
            # print("generate max ace now")
            for clan_member in clan_members_list:
                for ace_tier in clan_members_list[clan_member]["aces"]:
                    if ace_tier not in max_ace:
                        max_ace[ace_tier] = [
                            clan_members_list[clan_member]["name"],
                            clan_members_list[clan_member]["aces"][ace_tier],
                        ]
                    else:
                        if (
                            clan_members_list[clan_member]["aces"][ace_tier]
                            > max_ace[ace_tier][1]
                        ):
                            max_ace[ace_tier] = [
                                clan_members_list[clan_member]["name"],
                                clan_members_list[clan_member]["aces"][ace_tier],
                            ]
                for wr_tier in clan_members_list[clan_member]["tiers"]:
                    try:
                        wr = (
                            clan_members_list[clan_member]["tiers"][wr_tier]["wins"]
                            / clan_members_list[clan_member]["tiers"][wr_tier][
                                "battles"
                            ]
                            * 100
                        )
                    except:
                        wr = 0
                    if wr_tier not in max_wr:
                        max_wr[wr_tier] = [
                            clan_members_list[clan_member]["name"],
                            wr,
                            clan_members_list[clan_member]["tiers"][wr_tier]["battles"],
                        ]
                    else:
                        if wr > max_wr[wr_tier][1]:
                            max_wr[wr_tier] = [
                                clan_members_list[clan_member]["name"],
                                wr,
                                clan_members_list[clan_member]["tiers"][wr_tier][
                                    "battles"
                                ],
                            ]
                for dmg_tier in clan_members_list[clan_member]["tiers"]:
                    try:
                        dmg = (
                            clan_members_list[clan_member]["tiers"][dmg_tier][
                                "damage_dealt"
                            ]
                            / clan_members_list[clan_member]["tiers"][dmg_tier][
                                "battles"
                            ]
                        )
                    except:
                        dmg = 0
                    if dmg_tier not in max_dmg:
                        max_dmg[dmg_tier] = [
                            clan_members_list[clan_member]["name"],
                            dmg,
                            clan_members_list[clan_member]["tiers"][dmg_tier][
                                "battles"
                            ],
                        ]
                    else:
                        if dmg > max_dmg[dmg_tier][1]:
                            max_dmg[dmg_tier] = [
                                clan_members_list[clan_member]["name"],
                                dmg,
                                clan_members_list[clan_member]["tiers"][dmg_tier][
                                    "battles"
                                ],
                            ]
            # print(max_dmg)
            # print(max_ace)
            # print(max_wr)

            # clan_members_list=sorted(clan_members_list, key=lambda x: (x[3],-x[5]))
            out_list = "```{color}\n{text}\n\n".format(
                color=color_list, text=_("Ace medals")
            )
            out_list += "{:>5} {: <20} {: >8}\n".format(_("Tier"), _("Name"), _("ACE"))
            max_ace = dict(sorted(max_ace.items()))
            for i in max_ace:
                if max_ace[i][1]:
                    out_list += "{:>5} {: <20} {: >8}\n".format(
                        i, max_ace[i][0][:20], max_ace[i][1]
                    )
            out_list += "```"
            # out_total=out_list
            await ctx.send(out_list)

            out_list = "```{color}\n{text}\n\n".format(
                color=color_list, text=_("Winrate")
            )

            out_list += "{:>5} {: <20} {: >7} {: >8}\n".format(
                _("Tier"), _("Name"), _("Battles"), _("WR")
            )
            # max_wr=dict(sorted(max_wr.items(), key=lambda x: x[1],reverse=True))
            max_wr = dict(sorted(max_wr.items()))
            for i in max_wr:
                if max_wr[i][1]:
                    out_list += "{:>5} {: <20} {: >7} {: >8.2f}\n".format(
                        i, max_wr[i][0], max_wr[i][2], max_wr[i][1]
                    )
            out_list += "```"
            # out_total=out_list
            await ctx.send(out_list)

            out_list = "```{color}\n{text}\n\n".format(
                color=color_list, text=_("Damage")
            )
            out_list += "{:>5} {: <20} {: >7} {: >8}\n".format(
                _("Tier"), _("Name"), _("Battles"), _("DMG")
            )
            # max_dmg=dict(sorted(max_dmg.items(), key=lambda x: x[1],reverse=True))
            max_dmg = dict(sorted(max_dmg.items()))
            for i in max_dmg:
                if max_dmg[i][1]:
                    out_list += "{:>5} {: <20} {: >7} {: >8.2f}\n".format(
                        i, max_dmg[i][0], max_dmg[i][2], max_dmg[i][1]
                    )
            out_list += "``````Tier 0 are tanks not in WG tankopedia.```"
            # out_total=out_list
            await ctx.send(out_list)

            print(humanize.naturaldelta(tstart - time.time()))
            self.bot.logger.info("end of clan players charts task")


def setup(bot):
    bot.add_cog(ClanStats(bot))
