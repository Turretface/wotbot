# aiohttp rate limiting by gist:
# https://gist.github.com/pquentin/5d8f5408cdad73e589d85ba509091741

import aiosqlite as lite
import json
import datetime
import humanize
import asyncio
import time
import aiohttp
import sys
from pathlib import Path
import os.path
import os
import gzip
import zlib

import time
import shutil
import subprocess


os.chdir(os.path.dirname(os.path.abspath(__file__)))
log_file = "refresh-tokens.log"


# here we make a copy of the live database
with open("data/config.json") as json_data_file:
    cfg = json.load(json_data_file)

token = str(cfg["WargamingPrivateToken"])


class DB:
    def __init__(self):
        pass
        # self.con = lite.connect('data/users.db', detect_types=lite.PARSE_DECLTYPES)
        # self.con = lite.connect('file:data/users_copy.db?mode=ro', timeout=1000,uri=True, detect_types=lite.PARSE_DECLTYPES)

    async def get_users(self):

        async with lite.connect(
            "file:data/users.db?mode=ro",
            uri=True,
            detect_types=lite.core.sqlite3.PARSE_DECLTYPES,
        ) as db:
            db._conn.row_factory = lite.core.sqlite3.Row
            async with db.execute(
                "select account_id, region , token from users where token is not null"
            ) as cursor:
                rows = await cursor.fetchall()
                return rows

    async def store_token(self, user, access_token):
        w = {}
        w["user"] = user
        w["token"] = access_token

        # print(w)

        async with lite.connect(
            "file:data/users.db",
            uri=True,
            detect_types=lite.core.sqlite3.PARSE_DECLTYPES,
        ) as db:
            db._conn.row_factory = lite.core.sqlite3.Row
            # async with db.execute("select account_id, today from daily where region == $region and time_stamp > $since;",w) as cursor:
            await db.execute("update users set token=$token where account_id=$user", w)
            await db.commit()


class RateLimiter:
    """Rate limits an HTTP client that would make get() and post() caAAlls.

    Calls are rate-limited by host.
    https://quentin.pradet.me/blog/how-do-you-rate-limit-calls-with-aiohttp.html
    This class is not thread-safe."""

    RATE = 20
    MAX_TOKENS = 20

    def __init__(self, client):
        self.client = client
        self.tokens = self.MAX_TOKENS
        self.updated_at = time.monotonic()

    async def get(self, *args, **kwargs):
        await self.wait_for_token()
        return self.client.get(*args, **kwargs)

    async def post(self, *args, **kwargs):
        await self.wait_for_token()
        return self.client.post(*args, **kwargs)

    async def wait_for_token(self):
        while self.tokens < 1:
            self.add_new_tokens()
            await asyncio.sleep(1)
        self.tokens -= 1

    def add_new_tokens(self):
        now = time.monotonic()
        time_since_update = now - self.updated_at
        new_tokens = time_since_update * self.RATE
        if new_tokens > 1:
            self.tokens = min(self.tokens + new_tokens, self.MAX_TOKENS)
            self.updated_at = now


async def main(db):

    async with aiohttp.ClientSession(loop=loop) as client:
        client = RateLimiter(client)

        async def get_token(url, payload, account_id):
            # print(url)
            for i in range(3):
                async with await client.post(url, data=payload) as resp:
                    if resp.status != 200:
                        await resp.release()
                        print("retry on status {}".format(resp.status), i)
                        continue
                    data = await resp.json()
                    if data.get("status") == "ok":
                        # print("status ok")
                        return data
                    elif data.get("status") == "error":
                        await resp.release()
                        if data["error"]["code"] == 504:
                            print("retry on err 504", i)
                        else:
                            print(data["error"]["message"], account_id)
                            return

        u = await db.get_users()
        count = 0
        badtokencnt = 0
        for i in u:
            count += 1
            url = "https://api.worldoftanks.{region}/wot/auth/prolongate/".format(
                region=i["region"].replace("na", "com")
            )

            try:
                payload = {"application_id": token, "access_token": i["token"]}
                # print(payload, url)
                data = await get_token(url, payload, i["account_id"])
                if data is not None:
                    # print(data)
                    if data.get("data", None):
                        access_token = data["data"].get("access_token", None)
                        if access_token is not None:
                            # print("store: {}, was: {}".format(access_token, i["token"]))
                            await db.store_token(i["account_id"], access_token)
                        else:
                            badtokencnt += 1
                    else:
                        badtokencnt += 1
                else:
                    badtokencnt += 1
            except Exception as e:
                print(e)

        with open(log_file, "a") as the_file:
            the_file.write(
                "Fetched {} accounts, bad tokens: {}\n".format(count, badtokencnt)
            )


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    db = DB()
    with open(log_file, "a") as the_file:
        the_file.write(
            "{}\n".format(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        )

    loop.run_until_complete(main(db))

    with open(log_file, "a") as the_file:
        the_file.write(
            "{}\n\n".format(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        )
# This work is licensed under the terms of the MIT license.
# For a copy, see <https://opensource.org/licenses/MIT>.
